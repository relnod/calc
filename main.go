package main

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/relnod/calcgo/interpreter"
	"github.com/relnod/calcgo/lexer"
)

var TokenToHighlight map[lexer.TokenType]string = map[lexer.TokenType]string{
	lexer.TInt: "Number",
	lexer.TDec: "Number",
	lexer.TBin: "Number",
	lexer.THex: "Number",
	lexer.TExp: "Number",

	lexer.TVar: "Variable",

	lexer.TOpPlus:  "Operator",
	lexer.TOpMinus: "Operator",
	lexer.TOpMult:  "Operator",
	lexer.TOpDiv:   "Operator",
	lexer.TOpMod:   "Operator",
	lexer.TOpOr:    "Operator",
	lexer.TOpXor:   "Operator",
	lexer.TOpAnd:   "Operator",

	lexer.TFnSqrt: "Function",
	lexer.TFnSin:  "Function",
	lexer.TFnCos:  "Function",
	lexer.TFnTan:  "Function",

	lexer.TLParen: "Paren",
	lexer.TRParen: "Paren",

	lexer.TInvalidCharacter:           "Error",
	lexer.TInvalidCharacterInNumber:   "Error",
	lexer.TInvalidCharacterInVariable: "Error",
	lexer.TFnUnkown:                   "Error",
}

type Calculation struct {
	Result float64  `json:"result"`
	Errors []string `json:"errors"`
}

type Highlight struct {
	Type  string `json:"Type"`
	Start int    `json:"Start"`
	End   int    `json:"End"`
}

func handleCalculate(w http.ResponseWriter, r *http.Request) {
	val := r.FormValue("in")
	result, errs := interpreter.Interpret(val)

	calculation := Calculation{
		Result: result,
		Errors: []string{},
	}

	for _, err := range errs {
		calculation.Errors = append(calculation.Errors, err.Error())
	}

	writeStruct(w, calculation)
}

func handleHighlight(w http.ResponseWriter, r *http.Request) {
	val := r.FormValue("in")
	tokens := lexer.Lex(val)

	var highlights []Highlight
	for _, token := range tokens {
		highlights = append(highlights, Highlight{
			Type:  TokenToHighlight[token.Type],
			Start: token.Start,
			End:   token.End,
		})
	}

	writeStruct(w, highlights)
}

func writeStruct(w http.ResponseWriter, s interface{}) {
	json, err := json.Marshal(s)
	if err != nil {
		fmt.Println(err)
	}

	w.Write(json)
}

func main() {
	http.HandleFunc("/calculate", handleCalculate)
	http.HandleFunc("/highlight", handleHighlight)
	http.Handle("/", http.FileServer(http.Dir("static/")))

	http.ListenAndServe(":8080", nil)
}
