/// <reference path="../node_modules/@types/jquery/index.d.ts" />

import * as editor from "./editor/editor"
import * as dom from "./dom/element"

class Calculator {
    private editor: editor.Editor;
    private result: any;

    constructor() {
        this.editor = new editor.Editor("#editor");
        this.editor.on('submit', (e) => this.onSubmit(e));
        this.editor.on('change', (e) => this.onChange(e));

        this.result = $('#result');
    }

    private onSubmit(val) {
        $.ajax({
            url: "/calculate",
            method: "POST",
            data: {
                in: val
            },
            success: (r) => {
                r = JSON.parse(r);
                if (r.errors.length > 0) {
                    this.result.html("")

                    for (let err of r.errors) {
                        let el = new ResultError(err);
                        this.result.append(el.getEl());
                    }
                } else {
                    this.result.html(r.result)
                }
            }
        });

        return false;
    }

    private onChange(val) {
        $.ajax({
            url: "/highlight",
            method: "POST",
            data: {
                in: val
            },
            success: (r) => this.editor.highlight(JSON.parse(r))
        });

        return false;
    }
}

class ResultError extends dom.DomElement {
    constructor(err: string) {
        super();
        this.el.html(err);
    }

    protected render() {
        return '<div class="error"></div>';
    }
}


window.onload = () => new Calculator();
