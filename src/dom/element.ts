export class DomElement {
    protected el;

    constructor() {
        this.el = $(this.render());
    }

    protected render() : string {
        return '';
    }

    public getEl() {
        return this.el;
    }

    public html(str?: string) : string {
        if (str || str == "") {
            return this.el.html(str);
        }

        return this.el.html();
    }

    public height(height?: number) : number {
        if (height) {
            return this.el.height(height);
        }

        return this.el.height();
    }

    public width(width?: number) : number {
        if (width) {
            return this.el.width(width);
        }

        return this.el.width(width);
    }
}
