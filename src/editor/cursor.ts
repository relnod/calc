import * as element from "./../dom/element"

export enum Mode {
    LINE = "line",
    BLOCK = "block",
}

enum CursorState {
    VISIBLE,
    HIDDEN,
}

export class Cursor extends element.DomElement {
    private state: CursorState;
    private timer: any;

    constructor() {
        super();
        this.setMode(Mode.LINE);
        this.el.addClass('initial');
    }

    protected render() {
        return '<div class="cursor"></div>';
    }

    public setPos(pos: number) {
        if (pos == 0) {
            this.el.addClass('initial');
            return;
        }
        this.el.removeClass('initial');

        this.el.css({
            position: "absolute",
            left: pos + "px"
        }).show();
    }

    private blink() {
        switch(this.state) {
            case CursorState.VISIBLE:
                this.state = CursorState.HIDDEN;
                this.el.hide();
                return;
            case CursorState.HIDDEN:
                this.state = CursorState.VISIBLE;
                this.el.show();
                return;

        }
    }

    public show() {
        this.el.show();
        this.state = CursorState.VISIBLE;

        clearInterval(this.timer);
        this.timer = setInterval(() => (this.blink()), 500);
    }

    public hide() {
        this.el.hide();
        clearInterval(this.timer);
    }

    public setMode(mode: Mode) {
        this.el.removeClass(Mode.BLOCK);
        this.el.removeClass(Mode.LINE);

        this.el.addClass(mode);
    }
}
