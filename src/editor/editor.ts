import * as cursor from "./cursor"
import * as grid from "./grid"
import * as storage from "./storage"
import * as element from "./../dom/element"

enum EditorMode {
    INSERT,
    NORMAL,
    VISUAL,
    REPLACE,
    REPLACE_SINGLE,
}

export class Editor {
    private el: any;

    private storage: storage.Storage;
    private grid: grid.Grid;
    private cursor: cursor.Cursor;

    private pos: number;

    private actionCbs: ((any) => any)[]

    private mode: EditorMode;

    private prevKey: string;
    private prevVal: string;
    private prevPos: number;

    private startVisual: number;

    public constructor(selector: string) {
        this.el = $(selector);

        this.storage = new storage.Storage();

        this.grid = new grid.Grid(this.storage);
        this.el.append(this.grid.getEl());

        this.cursor = new cursor.Cursor();
        this.el.append(this.cursor.getEl());
        this.grid.html("a");
        this.cursor.height(this.grid.height());
        this.grid.html("");

        this.pos = 0;
        this.actionCbs = [];
        this.mode = EditorMode.INSERT;

        this.prevKey = "";
        this.prevVal = "";
        this.prevPos = 0;

        this.startVisual = 0;

        this.el.on('click', (e) => this.onClick(e.originalEvent));
        this.el.on('focusout', (e) => this.onFocusOut(e.originalEvent));
        this.el.on('keydown', (e) => this.onKeydown(e.originalEvent));
    }

    public highlight(highlights) {
        this.grid.addHighlights(highlights);
    }

    public on(action: string, fn: (any) => any) {
        this.actionCbs[action] = fn;
    }

    public onClick(e: any) {
        // console.log("click", e);

        let target = $(e.path[0]);
        if (target.hasClass('char')) {
            this.pos = Number(target.attr('data-id'));
            this.updateCursor();
        }

        this.el.focus();
        this.cursor.show();
    }

    public onFocusOut(e: MouseEvent) {
        this.cursor.hide();
    }

    public onKeydown(e: KeyboardEvent) {
        // console.log("keypress", e);

        switch (this.mode) {
            case EditorMode.INSERT:
                this.handleInsertMode(e.key);
                break;
            case EditorMode.NORMAL:
                this.handleNormalMode(e.key);
                break;
            case EditorMode.VISUAL:
                this.handleVisualMode(e.key);
                break;
            case EditorMode.REPLACE:
                this.handleReplaceMode(e.key);
                break;
            case EditorMode.REPLACE_SINGLE:
                this.handleReplaceSingleMode(e.key);
                break;
        }

        this.update();

        this.prevKey = e.key;
        this.prevVal = this.storage.val();
        this.prevPos = this.pos;
    }

    private handleInsertMode(key: string) {
        switch(key) {
            case "Escape":
                this.setMode(EditorMode.NORMAL);
                this.moveLeft();
                break;
            case "Enter":
                this.triggerSubmit();
                break;
            case "Backspace":
                if (this.pos == 0) {
                    return;
                }
                this.storage.remove(this.pos - 1);
                this.moveLeft();
                break;
            case "Delete":
                this.storage.remove(this.pos);
                break;
            case "ArrowLeft":
                this.moveLeft();
                break;
            case "ArrowRight":
                this.moveRight();
                break;
            case "Space":
                this.pos++;
                this.storage.addChar(" ", this.pos);
                break;
            case "Dead":
                this.pos++;
                this.storage.addChar("^", this.pos);
                break;
            default:
                if (key.length > 1) {
                    return;
                }

                this.pos++;
                this.storage.addChar(key, this.pos);
        }
    }

    private handleNormalMode(key: string) {
        switch(key) {
            case "a":
                this.setMode(EditorMode.INSERT);
                this.moveRight();
                break;
            case "A":
                this.setMode(EditorMode.INSERT);
                this.pos = this.storage.end();
                break;
            case "b":
                {
                    let pos = this.storage.findWordBackwards(this.pos);
                    switch(this.prevKey) {
                        case "c":
                            this.storage.removeRange(pos, this.pos);
                            this.setMode(EditorMode.INSERT);
                            break;
                        case "d":
                            this.storage.removeRange(pos, this.pos);
                            break;
                    }
                    this.pos = pos
                }

                break;
            case "c":
                switch(this.prevKey) {
                    case "c":
                        this.storage.clear();
                        this.pos = 0;
                        this.setMode(EditorMode.INSERT);
                }
                break;
            case "C":
                this.storage.removeRange(this.pos, this.storage.end());
                this.setMode(EditorMode.INSERT);
            break;
            case "d":
                switch(this.prevKey) {
                    case "d":
                        this.storage.clear();
                        this.pos = 0;
                        return;
                }
                break;
            case "D":
                this.storage.removeRange(this.pos, this.storage.end());
                break;
            case "h":
                this.moveLeft();
                break;
            case "i":
                this.setMode(EditorMode.INSERT);
                break;
            case "I":
                this.setMode(EditorMode.INSERT);
                this.pos = 0;
                break;
            case "l":
                this.moveRight();
                break;
            case "r":
                this.setMode(EditorMode.REPLACE_SINGLE);
                break;
            case "R":
                this.setMode(EditorMode.REPLACE);
                break;
            case "v":
                this.startVisual = this.pos;
                this.setMode(EditorMode.VISUAL);
                break;
            case "w":
                {
                    let pos = this.storage.findWord(this.pos);
                    switch(this.prevKey) {
                        case "c":
                            this.storage.removeRange(this.pos, pos);
                            this.setMode(EditorMode.INSERT);
                            return;
                        case "d":
                            this.storage.removeRange(this.pos, pos);
                            return;
                    }
                    this.pos = pos
                }

                break;
            case "x":
                this.storage.remove(this.pos);
                break;
            case "Dead":
                this.pos = 0;
                break;
            case "$":
                this.pos = this.storage.end();
                break;
        }
    }

    private handleVisualMode(key: string) {
        switch(key) {
            case "Escape":
                this.setMode(EditorMode.NORMAL);
                break;
            case "c":
                this.storage.removeRange(this.getVisualStart(), this.getVisualEnd());
                this.pos = this.getVisualStart();
                this.setMode(EditorMode.INSERT);
                break;
            case "d":
                this.storage.removeRange(this.getVisualStart(), this.getVisualEnd());
                this.pos = this.getVisualStart();
                this.setMode(EditorMode.NORMAL);
                break;
            case "h":
                this.moveLeft();
                break;
            case "l":
                this.moveRight();
                break;
        }
    }

    private handleReplaceMode(key: string) {
        switch(key) {
            case "Escape":
                this.setMode(EditorMode.NORMAL);
                break;
            default:
                if (key.length > 1) {
                    return;
                }

                this.storage.replaceChar(key, this.pos);
                this.moveRight();
        }
    }

    private handleReplaceSingleMode(key: string) {
        switch(key) {
            case "Escape":
                break;
            default:
                if (key.length > 1) {
                    return;
                }

                this.storage.replaceChar(key, this.pos);
        }

        this.setMode(EditorMode.NORMAL);
    }

    private triggerSubmit() {
        if (!("submit" in this.actionCbs)) {
            return;
        }

        this.actionCbs["submit"](this.storage.val());
    }

    private triggerChange() {
        if (!("change" in this.actionCbs)) {
            return;
        }

        this.actionCbs["change"](this.storage.val());
    }

    private update() {
        if (this.storage.val() != this.prevVal) {
            this.grid.update();
            this.triggerChange();
        }

        if (this.pos != this.prevPos) {
            this.updateCursor();

            if (this.mode == EditorMode.VISUAL) {
                this.grid.clearHighlight("Selection");
                this.grid.addHighlight({
                    Type: "Selection",
                    Start: this.getVisualStart(), 
                    End: this.getVisualEnd(),
                });
            }
        }
    }

    private updateCursor() {
        let width = this.grid.widthAt(this.pos);
        if (width != -1) {
            this.cursor.width(width);
        }

        this.cursor.setPos(this.grid.getOffsetAt(this.pos));
    }

    private getVisualStart() : number {
        return this.pos < this.startVisual ? this.pos : this.startVisual;
    }

    private getVisualEnd() : number {
        return this.pos < this.startVisual ? this.startVisual : this.pos + 1;
    }

    private moveLeft() {
        if (this.pos == 0) {
            return;
        }

        this.pos--;
    }

    private moveRight() {
        if (this.pos >= this.storage.end()) {
            return;
        }

        this.pos++;
    }

    private setMode(mode: EditorMode) {
        switch (this.mode) {
            case EditorMode.VISUAL:
                this.grid.clearHighlight("Selection");
                break;
        }

        this.mode = mode;

        switch (this.mode) {
            case EditorMode.INSERT:
                this.cursor.setMode(cursor.Mode.LINE);
                break;
            default:
                this.cursor.setMode(cursor.Mode.BLOCK);
        }
    }
}
