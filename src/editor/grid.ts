import * as element from "./../dom/element"
import * as storage from "./storage"

export class Grid extends element.DomElement {
    protected storage: storage.Storage;

    constructor(storage: storage.Storage) {
        super();

        this.storage = storage;
    }

    protected render() {
        return '<div class="grid"></div>';
    }

    public update() {
        this.el.html("");

        for (let i = 0; i < this.storage.size(); i++) {
            if (this.storage.at(i) == undefined) {
                continue;
            }

            let char = this.storage.at(i);
            if (char == " ") {
                char = "&nbsp";
            }

            let a = new Char(char, i);
            this.el.append(a.getEl());
        }
    }

    public widthAt(pos: number) : number {
        let el = this.getElAt(pos);
        if (!el) {
            return -1;
        }

        return el.width();
    }

    public getOffsetAt(pos: number) : number {
        if (pos >= this.storage.size()) {
            console.error("Trying to get postion at pos " + pos + "!");
            return 0;
        }

        if (pos == 0) {
            return 0;
        }

        let el = this.getElAt(pos);
        return el.position().left + el.width();
    }

    public addHighlight(highlight) {
        for (let i = highlight.Start; i < highlight.End; i++) {
            let el = this.getElAt(i + 1);
            if (!el) {
                continue;
            }
            el.addClass(highlight.Type);
        }
    }

    public addHighlights(highlights) {
        if (!highlights) {
            return;
        }

        for (let highlight of highlights) {
            this.addHighlight(highlight);
        }
    }

    public clearHighlight(highlight) {
        for (let i = 0; i < this.storage.size(); i++) {
            let el = this.getElAt(i + 1);
            if (!el) {
                continue;
            }
            el.removeClass(highlight);
        }
    }

    private getElAt(pos: number) : any {
        if (pos >= this.storage.size()) {
            return false;
        }

        return this.el.find('[data-id="' + (pos) + '"]');
    }
}

class Char extends element.DomElement {
    constructor(char: string, pos: number) {
        super();
        this.el.attr('data-id', pos);
        this.el.html(char);
    }

    render() {
        return '<span class="char"></span>';
    }
}
