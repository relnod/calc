export class Storage {
    protected storage: string[];

    constructor() {
        this.storage = [];
    }

    protected render() {
        return '<div class="grid"></div>';
    }

    public clear() {
        this.storage = [];
    }

    public replaceChar(char: string, pos: number) {
        this.storage[pos] = char;
    }

    public addChar(char: string, pos: number) {
        if (pos < this.size()) {
            this.shiftRightFrom(pos);
        }

        this.storage[pos] = char;
    }

    public remove(pos: number) {
        this.removeInternal(pos + 1, 1);
    }

    public removeRange(start: number, end: number) {
        this.removeInternal(start + 1, end - start + 1);
    }

    private removeInternal(pos: number, len: number) {
        if (pos > this.size() || len < 1) {
            console.error("removeInternal: Index out of bounds!");
            return;
        }

        this.storage.splice(pos, len);
    }

    public size() {
        return this.storage.length;
    }

    public end() {
        return this.size() - 1;
    }

    public at(pos: number) {
        return this.storage[pos];
    }

    public val() {
        return this.valFromTo(0, this.size());
    }

    public valFromTo(start: number, end: number) {
        let val = "";
        for (let i = start; i < end; i++) {
            if (!this.storage[i]) {
                continue;
            }

            val += this.storage[i];
        }

        return val;
    }

    private shiftRightFrom(pos: number) {
        for (let i = this.size(); i > pos; i--) {
            this.storage[i] = this.storage[i - 1];
        }
    }

    public findWord(currentPos: number) : number {
        let pos = this.findWordInternal(this.valFromTo(currentPos, this.size()));
        if (pos == -1) {
            return this.size() - 1;
        }

        console.log(currentPos, pos);
        return currentPos + pos;
    }

    public findWordBackwards(currentPos: number) : number {
        let val = reverse(this.valFromTo(0, currentPos));
        let pos = this.findWordInternal(val);
        if (pos == -1) {
            return 0;
        }

        return this.val.length - pos + currentPos;
    }

    private findWordInternal(str: string) : number {
        let regex = /[\. ]/;
        let match = regex.exec(str)
        if (match == null) {
            return -1;
        }

        return match.index + 1;
    }
}

function reverse(str: string) : string {
    return str.split("").reverse().join("");

}
