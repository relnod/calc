const path = require('path');

module.exports = {
  entry: [
      './src/calc.ts',
      './static/styles.less'
  ],
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/
      },
      {
        test: /\.less$/,
        loader: "style-loader!css-loader!less-loader"
      }
    ]
  },
  resolve: {
    extensions: [ '.tsx', '.ts', '.js', '.less' ]
  },
  output: {
    filename: 'calc.js',
    path: path.resolve(__dirname, 'static')
  }
};
